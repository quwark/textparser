Догоняем IE.
Согласно последним данным статистики, Firefox установлен у 12,46% пользователей интернета, в то время как Internet Explorer — у 82,1%. Год назад браузер от Microsoft занимал 90% рынка. Прогресс очевиден.

Появившись в 2004 году, Firefox стремительно набрал обороты. Во многом — благодаря макертинговой кампании Spread Firefox и его поклонникам пользователям, которые даже вкладывали деньги в сетевую рекламу. Свою лепту внёс и Google, вставив рекламные блоки некоторых связанных с Firefox продуктов и обеспечивая поддержку браузера во всех новых сервисах.

Впрочем, борьба далеко не окончена: в октябре будет представлен финальный релиз IE 7 с встроеннымми табами, поддержкой RSS и усиленной защитой от фишинга.