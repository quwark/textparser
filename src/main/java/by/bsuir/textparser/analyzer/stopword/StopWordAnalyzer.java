package by.bsuir.textparser.analyzer.stopword;

public interface StopWordAnalyzer {

    default boolean isStopWord(String word) {
        return false;
    }
}
