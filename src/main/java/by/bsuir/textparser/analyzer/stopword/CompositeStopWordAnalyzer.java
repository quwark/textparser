package by.bsuir.textparser.analyzer.stopword;

public class CompositeStopWordAnalyzer implements StopWordAnalyzer {

    private static final RussianStopWordAnalyzer RUSSIAN_STOP_WORD_ANALYZER = new RussianStopWordAnalyzer();
    private static final EnglishStopWordAnalyzer ENGLISH_STOP_WORD_ANALYZER = new EnglishStopWordAnalyzer();

    @Override
    public boolean isStopWord(String word) {
        return RUSSIAN_STOP_WORD_ANALYZER.isStopWord(word) || ENGLISH_STOP_WORD_ANALYZER.isStopWord(word);
    }
}
