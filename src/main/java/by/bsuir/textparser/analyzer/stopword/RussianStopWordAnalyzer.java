package by.bsuir.textparser.analyzer.stopword;

import java.net.URL;
import java.util.HashSet;
import java.util.Set;

import by.bsuir.textparser.utill.FileUtil;

public class RussianStopWordAnalyzer implements StopWordAnalyzer {

    private static final Set<String> RUSSIAN_STOP_WORDS;

    static {
        URL resource = RussianStopWordAnalyzer.class.getClassLoader().getResource("stopWords/rus.txt");
        RUSSIAN_STOP_WORDS = new HashSet<>(FileUtil.loadWordsFromFile(resource));
    }

    @Override
    public boolean isStopWord(String word) {
        return RUSSIAN_STOP_WORDS.contains(word.toLowerCase());
    }
}
