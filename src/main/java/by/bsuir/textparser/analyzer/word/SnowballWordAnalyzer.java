package by.bsuir.textparser.analyzer.word;

import by.bsuir.textparser.algorithm.snowball.SnowballStemmer;
import by.bsuir.textparser.algorithm.snowball.ext.RussianStemmer;

public class SnowballWordAnalyzer implements WordAnalyzer {

    private SnowballStemmer snowballStemmer = new RussianStemmer();

    @Override
    public String getBaseForm(String word) {
        snowballStemmer.setCurrent(word.toLowerCase());
        if (snowballStemmer.stem()) {
            return snowballStemmer.getCurrent();
        }
        return word.toLowerCase();
    }
}
