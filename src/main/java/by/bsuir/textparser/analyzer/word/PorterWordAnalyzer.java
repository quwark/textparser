package by.bsuir.textparser.analyzer.word;

import by.bsuir.textparser.algorithm.porter.PorterStemmer;

public class PorterWordAnalyzer implements WordAnalyzer {

    @Override
    public String getBaseForm(String word) {
        return PorterStemmer.stem(word.toLowerCase());
    }
}
