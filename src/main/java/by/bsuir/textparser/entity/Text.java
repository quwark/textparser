package by.bsuir.textparser.entity;

import by.bsuir.textparser.analyzer.stopword.CompositeStopWordAnalyzer;
import by.bsuir.textparser.analyzer.stopword.RussianStopWordAnalyzer;
import by.bsuir.textparser.analyzer.word.SnowballWordAnalyzer;
import by.bsuir.textparser.analyzer.stopword.StopWordAnalyzer;
import by.bsuir.textparser.analyzer.word.WordAnalyzer;

import java.util.ArrayList;
import java.util.List;

public class Text {

    private TextStatistic textStatistic;
    private WordAnalyzer wordAnalyzer;
    private StopWordAnalyzer stopWordAnalyzer;

    private String name;
    private List<Sentence> sentenceList;

    public Text(String name) {
        this(name, new ArrayList<>());
    }

    public Text(String name, List<Sentence> sentenceList) {
        this.name = name;
        this.sentenceList = sentenceList;
        this.wordAnalyzer = new SnowballWordAnalyzer();
        this.stopWordAnalyzer = new CompositeStopWordAnalyzer();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Sentence> getSentenceList() {
        return sentenceList;
    }

    public void setSentenceList(List<Sentence> sentenceList) {
        this.sentenceList = sentenceList;
    }

    public TextStatistic getTextStatistic() {
        if(textStatistic == null) {
            textStatistic = TextStatisticCreator.create(this);
        }
        return textStatistic;
    }

    public WordAnalyzer getWordAnalyzer() {
        return wordAnalyzer;
    }

    public void setWordAnalyzer(WordAnalyzer wordAnalyzer) {
        this.wordAnalyzer = wordAnalyzer;
    }

    public StopWordAnalyzer getStopWordAnalyzer() {
        return stopWordAnalyzer;
    }

    public void setStopWordAnalyzer(StopWordAnalyzer stopWordAnalyzer) {
        this.stopWordAnalyzer = stopWordAnalyzer;
    }

    @Override
    public String toString() {
        return name;
    }
}
