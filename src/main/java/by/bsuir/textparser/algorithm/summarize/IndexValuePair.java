package by.bsuir.textparser.algorithm.summarize;

/**
 * Convenience class for sorting arrays by
 * a floating point value associated with each element.
 */
class IndexValuePair implements Comparable<IndexValuePair>{

	double value;
	int index;
	
	// Comparison takes place on value.
	public int compareTo(IndexValuePair c) {
		return Double.compare(this.value, c.value);
	}
}