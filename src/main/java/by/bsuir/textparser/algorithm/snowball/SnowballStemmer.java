package by.bsuir.textparser.algorithm.snowball;

public abstract class SnowballStemmer extends SnowballProgram {

    public abstract boolean stem();
}
