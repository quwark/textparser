package by.bsuir.textparser.classifier.model;

import java.util.LinkedHashSet;
import java.util.Set;

public class Characteristic {

    private String name;
    private Set<CharacteristicValue> possibleValues;

    public Characteristic(String name) {
        this(name, new LinkedHashSet<>());
    }

    public Characteristic(String name, Set<CharacteristicValue> possibleValues) {
        this.name = name;
        this.possibleValues = possibleValues;
    }

    public String getName() {
        return name;
    }

    public Set<CharacteristicValue> getPossibleValues() {
        return possibleValues;
    }

    public void setPossibleValues(Set<CharacteristicValue> possibleValues) {
        this.possibleValues = possibleValues;
    }

    public void addPossibleValue(CharacteristicValue value) {
        possibleValues.add(value);
    }

    @Override
    public boolean equals(Object o) {
        return ((o instanceof Characteristic) && (this.name.equals(((Characteristic) o).getName())));
    }

    @Override
    public int hashCode() {
        return this.name.hashCode();
    }
}