package by.bsuir.textparser.ui.panel;

import by.bsuir.textparser.classifier.Classifier;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.io.File;
import java.util.Comparator;
import java.util.Map;

public class ClassifierPanel extends JPanel {

    private JLabel sourceTextLabel;
    private JTextArea resultTextArea;

    private File sourceFile;

    public ClassifierPanel() {
        initGUI();
    }

    private void initGUI() {
        sourceTextLabel = new JLabel("Text: ");

        resultTextArea = new JTextArea();
        resultTextArea.setLineWrap(true);
        resultTextArea.setWrapStyleWord(true);
        resultTextArea.setBorder(BorderFactory.createEmptyBorder(8, 8, 8,8 ));

        JScrollPane resultScrollPane = new JScrollPane();
        resultScrollPane.setAlignmentX(LEFT_ALIGNMENT);
        resultScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        resultScrollPane.getViewport().add(resultTextArea);

        JButton openFileButton = new JButton("Open...");
        openFileButton.setBounds(50, 60, 80, 30);
        openFileButton.addActionListener(e -> {
            JFileChooser fileChooser = new JFileChooser();
            fileChooser.setCurrentDirectory(new File("/home/mint/work/java/ver2/textparser/src/main/resources/text"));
            FileNameExtensionFilter filter = new FileNameExtensionFilter("Text files", "txt");
            fileChooser.setFileFilter(filter);

            int retVal = fileChooser.showDialog(ClassifierPanel.this, "Open File");
            if(retVal == JFileChooser.APPROVE_OPTION) {
                sourceFile = fileChooser.getSelectedFile();
                sourceTextLabel.setText("File: " + sourceFile.getAbsolutePath());
            }
        });

        JButton processButton = new JButton("Process");
        processButton.setBounds(50, 60, 80, 30);
        processButton.addActionListener(e -> {
            if(sourceFile == null) {
                return;
            }

            Map<String, Double> classification = Classifier.getClassification(sourceFile);
            StringBuilder result = new StringBuilder();

            classification.entrySet().stream()
                    .sorted(Comparator.comparing(Map.Entry::getValue, Comparator.reverseOrder()))
                    .limit(10)
                    .map(categoryToProbability -> String.format("%s -> (%1.3f%%)", categoryToProbability.getKey(),
                            Math.round(categoryToProbability.getValue() * 100000) / 1000.d))
                    .forEach(s -> result.append(s).append("\n"));
            resultTextArea.setText(result.toString());
        });

        JPanel topPanel = new JPanel();
        topPanel.setLayout(new GridLayout(2, 2));

        topPanel.add(openFileButton);
        topPanel.add(sourceTextLabel);
        topPanel.add(processButton);

        JPanel centerPanel = new JPanel();
        centerPanel.setLayout(new BorderLayout());

        centerPanel.add(resultScrollPane);

        setLayout(new BorderLayout());
        add(topPanel, BorderLayout.NORTH);
        add(centerPanel, BorderLayout.CENTER);
    }
}
