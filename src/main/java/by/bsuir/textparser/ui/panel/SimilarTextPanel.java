package by.bsuir.textparser.ui.panel;

import by.bsuir.textparser.entity.Text;
import by.bsuir.textparser.entity.TextGroup;
import by.bsuir.textparser.service.TextService;
import by.bsuir.textparser.service.TextServiceImpl;
import by.bsuir.textparser.utill.FileUtil;
import by.bsuir.textparser.utill.TextParser;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class SimilarTextPanel extends JPanel {

    private TextService ts;

    private JLabel sourceFileLabel;
    private JLabel rootDirectoryLabel;

    private File sourceFile;
    private File rootDirectory;


    private JTextArea resultTextArea;

    public SimilarTextPanel(TextService ts) {
        this.ts = ts;
        initGUI();
    }

    private void initGUI() {
        sourceFileLabel = new JLabel("Text to use");
        rootDirectoryLabel = new JLabel("Root directory");

        JButton sourceTextChooserButton = new JButton("Choose");
        sourceTextChooserButton.setBounds(50, 60, 80, 30);
        sourceTextChooserButton.addActionListener(e -> {
            JFileChooser fileChooser = new JFileChooser();
            fileChooser.setCurrentDirectory(new File("/home/mint/work/java/ver2/textparser/src/main/resources/text"));
            FileNameExtensionFilter filter = new FileNameExtensionFilter("Text files", "txt");
            fileChooser.setFileFilter(filter);

            int retVal = fileChooser.showDialog(SimilarTextPanel.this, "Open File");
            if(retVal == JFileChooser.APPROVE_OPTION) {
                sourceFile = fileChooser.getSelectedFile();
                sourceFileLabel.setText("File: " + sourceFile.getAbsolutePath());
            }
        });

        JButton rootDirectoryChooserButton = new JButton("Choose");
        rootDirectoryChooserButton.setBounds(50, 60, 80, 30);
        rootDirectoryChooserButton.addActionListener(e ->  {
            JFileChooser fileChooser = new JFileChooser();
            fileChooser.setCurrentDirectory(new File("/home/mint/work/java/ver2/textparser/src/main/resources/text"));
            fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

            int retVal = fileChooser.showDialog(SimilarTextPanel.this, "Open Directory");
            if(retVal == JFileChooser.APPROVE_OPTION) {
                rootDirectory = fileChooser.getSelectedFile();
                rootDirectoryLabel.setText("Directory: " + rootDirectory.getAbsolutePath());
            }
        });

        JButton processButton = new JButton("Process");
        processButton.setBounds(50, 60, 80, 30);
        processButton.addActionListener(e -> {
            if(sourceFile == null || rootDirectory == null) {
                return;
            }

            Text text = TextParser.buildText(sourceFile.getName(), FileUtil.loadTextFromFile(sourceFile.toPath()));
            List<File> collect = FileUtil.loadFilesFromDirectory(rootDirectory)
                    .stream().filter(f -> !sourceFile.equals(f)).collect(Collectors.toList());
            List<Text> collect1 = collect.stream().map(SimilarTextPanel::buildTextFormFile).collect(Collectors.toList());
            TextGroup similarText = ts.findSimilarText(text, collect1, 2);

            StringBuilder result = new StringBuilder();
            for(Text tx : similarText.getSimilarTextList()) {
                result.append(tx.getName()).append(Arrays.toString(tx.getTextStatistic().getKeyWords().toArray()));
                result.append("\n");
            }

            resultTextArea.setText(result.toString());
        });

        resultTextArea = new JTextArea();
        resultTextArea.setLineWrap(true);
        resultTextArea.setWrapStyleWord(true);
        resultTextArea.setBorder(BorderFactory.createEmptyBorder(8, 8, 8,8 ));

        JScrollPane resultScrollPane = new JScrollPane();
        resultScrollPane.setAlignmentX(LEFT_ALIGNMENT);
        resultScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        resultScrollPane.getViewport().add(resultTextArea);

        JPanel topPanel = new JPanel();
        topPanel.setLayout(new GridLayout(3, 2));
        topPanel.add(sourceTextChooserButton);
        topPanel.add(sourceFileLabel);
        topPanel.add(rootDirectoryChooserButton);
        topPanel.add(rootDirectoryLabel);
        topPanel.add(processButton);

        JPanel centerPanel = new JPanel();
        centerPanel.setLayout(new BorderLayout());
        centerPanel.add(resultScrollPane);

        setLayout(new BorderLayout());
        add(topPanel, BorderLayout.NORTH);
        add(centerPanel, BorderLayout.CENTER);
    }

    private static Text buildTextFormFile(File file) {
        String str = FileUtil.loadTextFromFile(file.toPath());
        return TextParser.buildText(file.getName(), str);
   }
}
