package by.bsuir.textparser.ui.panel;

import by.bsuir.textparser.entity.Text;
import by.bsuir.textparser.entity.Word;
import by.bsuir.textparser.service.TextService;
import by.bsuir.textparser.service.TextServiceImpl;
import by.bsuir.textparser.utill.FileUtil;
import by.bsuir.textparser.utill.TextParser;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.text.Document;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Arrays;
import java.util.List;

public class SummarizePanel extends JPanel {
    private TextService ts;

    private JTextArea sourceTextArea;
    private JTextArea keywordTextArea;
    private JTextArea summaryTextArea;

    private JSlider percentSlider;
    private JLabel percentLabel;

    private JLabel sourceWordsLabel;
    private JLabel sourceCharsLabel;
    private JLabel sourceLinesLabel;
    private JLabel summaryWordsLabel;
    private JLabel summaryCharsLabel;
    private JLabel summaryLinesLabel;

    public SummarizePanel(TextService ts) {
        this.ts = ts;
        initUI();
    }



    public void initUI() {
        JButton openTextButton = new JButton("Open...");
        openTextButton.setBounds(50, 60, 80, 30);
        openTextButton.addActionListener(new OpenActionListener());

        sourceTextArea = new JTextArea();
        sourceTextArea.setLineWrap(true);
        sourceTextArea.setWrapStyleWord(true);
        sourceTextArea.setBorder(BorderFactory.createEmptyBorder(8, 8, 8,8));

        JScrollPane sourceScrollPane = new JScrollPane();
        sourceScrollPane.setAlignmentX(LEFT_ALIGNMENT);
        sourceScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        sourceScrollPane.getViewport().add(sourceTextArea);

        keywordTextArea = new JTextArea();
        keywordTextArea.setLineWrap(true);
        keywordTextArea.setWrapStyleWord(true);
        keywordTextArea.setBorder(BorderFactory.createEmptyBorder(8, 8, 8,8));

        JScrollPane keywordScrollPane = new JScrollPane();
        keywordScrollPane.setAlignmentX(LEFT_ALIGNMENT);
        keywordScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        keywordScrollPane.getViewport().add(keywordTextArea);

        summaryTextArea = new JTextArea();
        summaryTextArea.setLineWrap(true);
        summaryTextArea.setWrapStyleWord(true);
        summaryTextArea.setBorder(BorderFactory.createEmptyBorder(8, 8, 8,8 ));

        JScrollPane summaryScrollPane = new JScrollPane();
        summaryScrollPane.setAlignmentX(LEFT_ALIGNMENT);
        summaryScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        summaryScrollPane.getViewport().add(summaryTextArea);
        summaryScrollPane.setPreferredSize(new Dimension(sourceScrollPane.getWidth(), 10));

        JLabel sourceTitleLabel = new JLabel("Source");
        sourceTitleLabel.setAlignmentX(LEFT_ALIGNMENT);
        JLabel keywordTitleLabel = new JLabel("Keyword");
        keywordTitleLabel.setAlignmentX(LEFT_ALIGNMENT);
        JLabel summaryTitleLabel = new JLabel("Summary");
        summaryTitleLabel.setAlignmentX(LEFT_ALIGNMENT);

        sourceTextArea.getDocument().addDocumentListener(new TextChangeListener());
        sourceTextArea.getDocument().putProperty("name", "source");
        summaryTextArea.getDocument().addDocumentListener(new TextChangeListener());
        summaryTextArea.getDocument().putProperty("name", "summary");


        sourceCharsLabel = new JLabel("Characters: ");
        sourceCharsLabel.setAlignmentX(Component.LEFT_ALIGNMENT);
        sourceWordsLabel = new JLabel("Words: ");
        sourceWordsLabel.setAlignmentX(Component.LEFT_ALIGNMENT);
        sourceLinesLabel = new JLabel("Lines: ");
        sourceLinesLabel.setAlignmentX(Component.LEFT_ALIGNMENT);

        summaryCharsLabel = new JLabel("Characters: ");
        summaryCharsLabel.setAlignmentX(Component.LEFT_ALIGNMENT);
        summaryWordsLabel = new JLabel("Words: ");
        summaryWordsLabel.setAlignmentX(Component.LEFT_ALIGNMENT);
        summaryLinesLabel = new JLabel("Lines: ");
        summaryLinesLabel.setAlignmentX(Component.LEFT_ALIGNMENT);

        JPanel leftPanel = new JPanel();
        leftPanel.setLayout(new BoxLayout(leftPanel, BoxLayout.Y_AXIS));
        leftPanel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 10));
        JPanel rightPanel = new JPanel();
        rightPanel.setLayout(new BoxLayout(rightPanel, BoxLayout.Y_AXIS));
        rightPanel.setBorder(BorderFactory.createEmptyBorder(0, 10, 0, 8));

        leftPanel.add(openTextButton);
        leftPanel.add(Box.createRigidArea(new Dimension(0, 5)));
        leftPanel.add(sourceTitleLabel);
        leftPanel.add(Box.createRigidArea(new Dimension(0, 5)));
        leftPanel.add(sourceScrollPane);
        leftPanel.add(Box.createRigidArea(new Dimension(0, 10)));
        leftPanel.add(sourceCharsLabel);
        leftPanel.add(Box.createRigidArea(new Dimension(0, 5)));
        leftPanel.add(sourceWordsLabel);
        leftPanel.add(Box.createRigidArea(new Dimension(0, 5)));
        leftPanel.add(sourceLinesLabel);

        rightPanel.add(keywordTitleLabel);
        rightPanel.add(Box.createRigidArea(new Dimension(0, 5)));
        rightPanel.add(keywordScrollPane);
        rightPanel.add(Box.createRigidArea(new Dimension(0, 10)));
        rightPanel.add(summaryTitleLabel);
        rightPanel.add(Box.createRigidArea(new Dimension(0, 5)));
        rightPanel.add(summaryScrollPane);
        rightPanel.add(Box.createRigidArea(new Dimension(0, 10)));
        rightPanel.add(summaryCharsLabel);
        rightPanel.add(Box.createRigidArea(new Dimension(0, 5)));
        rightPanel.add(summaryWordsLabel);
        rightPanel.add(Box.createRigidArea(new Dimension(0, 5)));
        rightPanel.add(summaryLinesLabel);

        JPanel centralPanel = new JPanel();
        centralPanel.setLayout(new GridLayout(1, 2));

        centralPanel.add(leftPanel, BorderLayout.WEST);
        centralPanel.add(rightPanel, BorderLayout.EAST);

        percentSlider = new JSlider();
        percentSlider.setBorder(BorderFactory.createTitledBorder("Summary Length"));
        percentSlider.setValue(20);
        percentSlider.setMajorTickSpacing(20);
        percentSlider.setMinorTickSpacing(5);
        percentSlider.setPaintTicks(true);
        percentSlider.addChangeListener(new SliderChangeListener());

        percentLabel = new JLabel("20%");

        JButton summarizeButton = new JButton("Summarize");
        summarizeButton.setBounds(50, 60, 80, 30);
        summarizeButton.addActionListener(new SummarizeActionListener());

        JPanel bottomPanel = new JPanel();
        bottomPanel.setLayout(new BoxLayout(bottomPanel, BoxLayout.X_AXIS));
        bottomPanel.setBorder(BorderFactory.createEmptyBorder(20, 0, 0, 0));

        bottomPanel.add(percentSlider);
        bottomPanel.add(percentLabel);
        bottomPanel.add(Box.createHorizontalGlue());
        bottomPanel.add(summarizeButton);

        setLayout(new BorderLayout());
        setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));

        add(centralPanel, BorderLayout.CENTER);
        add(bottomPanel, BorderLayout.SOUTH);
    }

    private class OpenActionListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            JFileChooser fileChooser = new JFileChooser();
            FileNameExtensionFilter filter = new FileNameExtensionFilter("Text files", "txt");
            fileChooser.setCurrentDirectory(new File("/home/mint/work/java/ver2/textparser/src/main/resources/text"));
            fileChooser.setFileFilter(filter);

            int retVal = fileChooser.showDialog(SummarizePanel.this, "Open File");
            if(retVal == JFileChooser.APPROVE_OPTION) {
                File file = fileChooser.getSelectedFile();
                String text = FileUtil.loadTextFromFile(file.toPath());
                sourceTextArea.setText(text);
            }
        }
    }

    private class SliderChangeListener implements ChangeListener {
        @Override
        public void stateChanged(ChangeEvent e) {
            JSlider slider = (JSlider) e.getSource();
            int val = slider.getValue();
            percentLabel.setText(val + "%");
        }
    }

    private class SummarizeActionListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            Text text = TextParser.buildText("1", sourceTextArea.getText());

            int value = percentSlider.getValue();
            String summary = ts.summarize(text, value);
            List<Word> keyWords = text.getTextStatistic().getKeyWords();

            keywordTextArea.setText(Arrays.toString(keyWords.toArray()));
            summaryTextArea.setText(summary);
        }
    }

    private class LoadTextActionListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {

        }
    }

    private class TextChangeListener implements DocumentListener {
        @Override
        public void insertUpdate(DocumentEvent e) {
            updateStats(e);
        }

        @Override
        public void removeUpdate(DocumentEvent e) {
            updateStats(e);
        }

        @Override
        public void changedUpdate(DocumentEvent e) {
            updateStats(e);
        }

        private void updateStats(DocumentEvent e) {
            Document doc = e.getDocument();
            String source = (String) doc.getProperty("name");
            String text = (source.equals("source"))
                    ? sourceTextArea.getText()
                    : summaryTextArea.getText();

            // Stats counter based on wc command sample implementation.
            // http://www.gnu.org/software/cflow/manual/html_node/Source-of-wc-command.html
            int ccount = 0;
            int wcount = 0;
            int lcount = 0;

            int pos = 0;
            while (pos < text.length()) {
                while (pos < text.length()) {
                    if (Character.isLetter(text.charAt(pos))) {
                        wcount++;
                        break;
                    }
                    ccount++;
                    if (text.charAt(pos) == '\n') {
                        lcount++;
                    }
                    pos++;
                }

                while (pos < text.length()) {
                    ccount++;
                    if (text.charAt(pos) == '\n') {
                        lcount++;
                    }

                    if (!Character.isLetter(text.charAt(pos))) {
                        break;
                    }
                    pos++;
                }
                pos++;
            }

            // Put figures into labels.
            if (source.equals("source")) {
                // Update source statistics widgets.
                sourceCharsLabel.setText("Characters: " + ccount);
                sourceWordsLabel.setText("Words: " + wcount);
                sourceLinesLabel.setText("Lines: " + lcount);
            }
            else {
                // Update summary statistics widgets.
                summaryCharsLabel.setText("Characters: " + ccount);
                summaryWordsLabel.setText("Words: " + wcount);
                summaryLinesLabel.setText("Lines: " + lcount);
            }
        }
    }
}
