package by.bsuir.textparser.ui;

import by.bsuir.textparser.service.TextService;
import by.bsuir.textparser.service.TextServiceImpl;
import by.bsuir.textparser.ui.panel.ClassifierPanel;
import by.bsuir.textparser.ui.panel.SimilarTextPanel;
import by.bsuir.textparser.ui.panel.SummarizePanel;

import javax.swing.*;

public class MainWindow extends JFrame {

    private TextService ts;

    public MainWindow(TextService ts) {
        this.ts = ts;
        initUI();
    }



    public void initUI() {
        JTabbedPane tabbedPane = new JTabbedPane();

        tabbedPane.addTab("Similar Text", new SimilarTextPanel(ts));
        tabbedPane.addTab("Summary", new SummarizePanel(ts));
        tabbedPane.addTab("Classifier", new ClassifierPanel());

        add(tabbedPane);
        setTitle("Text");
        setSize(800, 600);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }
}

